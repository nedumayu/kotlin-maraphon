fun main() {
    val helloWorld = Thread {
        Thread.sleep(1000)
        println("World")
    }
    helloWorld.start()

    Thread.sleep(2000)
    println("Hello,")
    helloWorld.join()
}
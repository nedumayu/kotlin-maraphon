import kotlin.system.measureTimeMillis
import kotlinx.coroutines.*

suspend fun firstNumber(): Int {
    delay(2000)
    return 1
}

suspend fun secondNumber(): Int {
    delay(3000)
    return 665
}

fun main() = runBlocking {
    val sequentialCall = measureTimeMillis {
        val firstNum = firstNumber()
        val secondNum = secondNumber()
        val sum = firstNum + secondNum
        println("Сумма при последовательном вызове = $sum")
    }
    println("Время при последовательном вызове = $sequentialCall милисекунд")

    val asyncCall = measureTimeMillis {
        val firstNum = async { firstNumber() }
        val secondNum = async { secondNumber() }
        val sum = firstNum.await() + secondNum.await()
        println("Сумма при асинхронном вызове = $sum")
    }
    println("Время при асинхронном вызове = $asyncCall милисекунд")

    repeat(3) {
        delay(1000)
        println("I'm sleeping $it...")
    }
    println("main: I'm tired of waiting! I'm running finally")
    println("main: Now I can quit.")
}